const express = require("express");
const app = express();

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Header",
    "Origin,X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET,POST,PATCH,DELETE,OPTIONS"
  );
  next();
});

app.get("/api/posts", (req, res, next) => {
  const posts = [
    {
      id: "kjdj48i",
      title: "Post One",
      content: "This is the post one"
    },
    {
      id: "oisafeo8ondvd",
      title: "Post VdV",
      content: "This is the post VdV"
    }
  ];
  res.status(200).json(posts);
});

module.exports = app;
