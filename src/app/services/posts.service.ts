import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { of } from "rxjs";
import { Posts } from "../models/Posts";
import { map } from "rxjs/operators";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Title } from "@angular/platform-browser";
import { typeofExpr } from "@angular/compiler/src/output/output_ast";
import { Content } from "@angular/compiler/src/render3/r3_ast";
import { Router } from "@angular/router";
@Injectable({
  providedIn: "root"
})
export class PostsService {
  posts: Posts[] = [];
  post;
  postCount;
  private toEditPost = new Subject<any>();
  private postUpdated = new Subject<Posts[]>();

  constructor(private http: HttpClient, private router: Router) {}

  getPostCount() {
    return this.http.get<any>("http://localhost:5000/api/getPostCount");
    // .pipe(
    //   map(post => {
    //     return post.data.map(post => {
    //       return post;
    //     });
    //   })
    // );
    // .subscribe(post => {
    //   console.log(post);
    //   this.postCount = post;
    //   // return this.postCount.asObservable();
    // });
  }

  getPosts(postPerPage, currentPage) {
    const queryParams = `?pageSize=${postPerPage}&currentPage=${currentPage}`;
    this.http
      .get<any>("http://localhost:5000/api/getPosts" + queryParams)
      .pipe(
        map(postData => {
          console.log(postData);
          return {
            posts: postData.posts.map(post => {
              return {
                title: post.title,
                content: post.content,
                id: post._id,
                imagePath: post.image
              };
            })
          };
        })
      )
      .subscribe(posts => {
        // console.log(posts);
        this.posts = posts.posts;
        console.log(this.posts);
        this.postUpdated.next([...this.posts]);
      });
  }

  getPostUpdateListen() {
    return this.postUpdated.asObservable();
  }

  addPosts(post: Posts) {
    console.log(post);
    // const postData: Posts = post;
    const postData = new FormData();
    postData.append("title", post.title);
    postData.append("content", post.content);
    postData.append("image", post.image, post.title);
    console.log(post);
    this.http
      .post<any>("http://localhost:5000/api/addPost", postData)
      .subscribe(resPost => {
        console.log(resPost);
        const upost: Posts = {
          id: resPost.post._id,
          title: post.title,
          content: post.content,
          imagePath: resPost.post.image
        };
        console.log(upost);
        const id = resPost.id;
        upost.id = id;
        this.posts.push(upost);
        this.postUpdated.next([...this.posts]);
        this.router.navigate(["/"]);
      });
  }

  deletePost(id) {
    return this.http.delete<{ message: string; post: Posts }>(
      "http://localhost:5000/api/deletePost/" + id
    );
    // .subscribe(data => {
    //   console.log(data);
    //   const updatedPosts = this.posts.filter(post => {
    //     return post.id !== id;
    //   });
    //   this.posts = updatedPosts;
    //   this.postUpdated.next([...this.posts]);
    //   this.router.navigate(["/"]);
    // });
  }

  getPost(id: string) {
    return this.http.get<any>("http://localhost:5000/api/getSinglePost/" + id);
  }

  updatePost(id: string, post: Posts) {
    console.log(post);
    let postData;
    // const Post = { id: id, post: post };
    // const editedPost = {
    //   id: id,
    //   title: post.title,
    //   content: post.content,
    //   imagePath: post.image
    // };
    if (typeof (post.image === "object")) {
      postData = new FormData();
      postData.append("title", post.title);
      postData.append("content", post.content);

      postData.append("image", post.image);
      console.log(postData);
    } else {
      postData = {
        id: id,
        title: post.title,
        content: post.content,
        imagePath: post.image
      };
      console.log(postData);
    }
    this.http
      .put<any>("http://localhost:5000/api/updatePost/" + id, postData)
      .subscribe(post => {
        console.log(post.post);
        const updatedPosts = [...this.posts];
        const oldPostIndex = updatedPosts.findIndex(p => p.id === id);
        console.log(oldPostIndex);
        const upost = {
          id: id,
          title: post.post.title,
          content: post.post.content,
          imagePath: post.post.image
          // post.post.image
        };
        updatedPosts[oldPostIndex] = upost;
        console.log(updatedPosts);
        this.posts = updatedPosts;
        this.postUpdated.next([...this.posts]);
      });
  }
}
