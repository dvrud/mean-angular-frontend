export interface Posts {
  id?: string;
  title?: string;
  content?: string;
  image?: string;
  imagePath?: string;
}
