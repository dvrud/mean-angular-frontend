import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { PostCreateComponent } from "./components/post-create/post-create.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { PostListComponent } from "./components/post-list/post-list.component";
import { FlashMessagesModule } from "angular2-flash-messages";
import { AppRoutingModule } from "./app-routing.module";
// To use angular material
// import { MatInputModule, MatCardModule } from "@angular/material";

@NgModule({
  declarations: [
    AppComponent,
    PostCreateComponent,
    NavbarComponent,
    PostListComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FlashMessagesModule.forRoot(),
    HttpClientModule,
    AppRoutingModule

    // MatInputModule,
    // MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
