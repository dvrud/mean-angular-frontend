import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { PostsService } from "../../services/posts.service";
import { Posts } from "../../models/Posts";
import { map } from "rxjs/operators";
import { Subscription } from "rxjs";
@Component({
  selector: "app-post-list",
  templateUrl: "./post-list.component.html",
  styleUrls: ["./post-list.component.css"]
})
export class PostListComponent implements OnInit, OnDestroy {
  posts: Posts[] = [];
  postsSub: Subscription;
  postsPerPage = 2;
  currentPage = 1;
  postCount;
  i;
  j;
  arr = [];
  // posts: Posts[] = [
  //   { title: "FIrst Post", content: "This is the content of the first Post" },
  //   { title: "Second Post", content: "This is the content of the second Post" },
  //   { title: "Third Post", content: "This is the content of the third Post" }
  // ];

  constructor(private postsService: PostsService) {}

  ngOnInit() {
    this.postsService.getPostCount().subscribe(post => {
      this.postCount = post.data;

      for (
        this.i = 1;
        this.i <= Math.ceil(this.postCount.length / 2);
        this.i++
      ) {
        this.arr.push(this.i);
      }
      console.log(this.arr);
    });

    this.postsService.getPosts(this.postsPerPage, this.currentPage);
    this.postsSub = this.postsService
      .getPostUpdateListen()
      .subscribe((posts: Posts[]) => {
        this.posts = posts;

        console.log(this.posts.length);
      });
  }

  onChangePage(pageIndex) {
    console.log(pageIndex);
    this.currentPage = pageIndex;
    this.postsService.getPosts(this.postsPerPage, this.currentPage);
  }

  ngOnDestroy() {
    this.postsSub.unsubscribe();
  }

  onDelete(postId: string) {
    this.postsService.deletePost(postId).subscribe(post => {
      this.postsService.getPosts(this.postsPerPage, this.currentPage);
    });
  }
}
