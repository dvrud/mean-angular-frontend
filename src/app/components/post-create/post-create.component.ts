import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  ViewChild
} from "@angular/core";
// import { mimeType } from "./mime-type.validator";
import { PostsService } from "../../services/posts.service";
import { Posts } from "../../models/Posts";
import { FlashMessagesService } from "angular2-flash-messages";
import {
  RouterModule,
  Router,
  ActivatedRoute,
  ParamMap
} from "@angular/router";
// import { Subscriber } from "rxjs";
import { Subscription, from } from "rxjs";
import { FormGroup, FormControl, Validators } from "@angular/forms";
@Component({
  selector: "app-post-create",
  templateUrl: "./post-create.component.html",
  styleUrls: ["./post-create.component.css"]
})
export class PostCreateComponent implements OnInit {
  posts: Posts = {
    title: "",
    content: ""
  };
  mode: string = "create";
  editpost: Posts = {
    id: "",
    title: "",
    content: "",
    imagePath: ""
  };
  imgPreview;
  postId: string;
  // To work with reactive forms.
  form: FormGroup;
  // post : Subscriber<>();
  // @Output() postCreated = new EventEmitter();
  // @ViewChild("postForm") form: any;

  constructor(
    private postsService: PostsService,
    private flashMessage: FlashMessagesService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    // REactive forms tasks.
    this.form = new FormGroup({
      title: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(3)]
      }),
      content: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(10)]
      }),
      image: new FormControl(null, {
        validators: [Validators.required]
        // asyncValidators: [mimeType]
      })
    });
    //
    this.route.paramMap.subscribe((param: ParamMap) => {
      if (param.has("id")) {
        this.mode = "edit";
        this.postId = param.get("id");
        console.log(this.mode, this.postId);
        // this.postsService.getPost(this.postId);
        this.postsService.getPost(this.postId).subscribe(post => {
          console.log(post.post);
          this.editpost = {
            id: post.post._id,
            title: post.post.title,
            content: post.post.content,
            // imagePath: post.post.image
            imagePath: post.post.image
          };
          this.form.setValue({
            title: this.editpost.title,
            content: this.editpost.content,
            image: this.editpost.imagePath
          });
        });
      } else {
        this.mode = "create";
        this.postId = null;
      }
    });
  }

  onAddPost() {
    if (this.form.invalid) {
      this.flashMessage.show("Please fill out the form properly", {
        cssClass: "alert-danger",
        timeout: 3000
      });
    } else {
      if (this.mode === "create") {
        console.log(this.form.value);
        this.postsService.addPosts(this.form.value);
        this.flashMessage.show("Post Successfully added", {
          cssClass: "alert-success",
          timeout: 3000
        });
        // This method is used to reset the form ..... means to clear the enterd value in the form.
      } else {
        this.postsService.updatePost(this.postId, this.form.value);
      }
      this.form.reset();
      this.router.navigate(["/"]);
    }
  }

  onImagePicked(e: Event) {
    const file: any = (e.target as HTMLInputElement).files[0];
    // let arr = [];
    this.form.patchValue({ image: file });
    this.form.get("image").updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imgPreview = reader.result;
    };
    // file.forEach(file => {
    reader.readAsDataURL(file);
    // });
    // for (let i = 0; i <= file.length; i++) {
    //   arr.push(file[i]);
    // }
    // console.log(arr);
    // arr.forEach(file => {
    //   reader.readAsDataURL(file);
    // });
  }
}
